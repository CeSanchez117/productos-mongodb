package mis.pruebas.productosmongodb.controlador;

import mis.pruebas.productosmongodb.modelo.Producto;
import mis.pruebas.productosmongodb.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/almacen/v2/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    //GET
    @GetMapping
    public List<Producto> getProducts(){
        return  this.servicioProducto.getAll();
    }

    //GET ID
    @GetMapping("/{id}")
    public Producto getProductById(@PathVariable String id){
        return this.servicioProducto.getById(id);
    }

    //POST
    @PostMapping
    public void createProducts(@RequestBody Producto producto){
        producto.setId(null);
        this.servicioProducto.addProduct(producto);
    }

    //UPDATE
    @PutMapping("/productoUpdate")
    public ResponseEntity putProductos(@RequestBody Producto producto){
        return this.servicioProducto.updateProducto(producto);
    }

    //UPDATE
    @PutMapping("/{id}")
    public ResponseEntity putProductosById(@PathVariable String id, @RequestBody Producto producto){
        final Producto p = this.servicioProducto.getById(id);
        if (p == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }else{
            return this.servicioProducto.saveProductoById(id, producto);
        }
    }

    //Delete
    @DeleteMapping("/deleteProductos")
    public ResponseEntity deleteProductos(@RequestBody Producto productoToDelete){
        return this.servicioProducto.delete(productoToDelete);
    }

    //GET
    @GetMapping("/getByPrice")
    public List<Producto> getProductsByprice(@RequestParam(required = false) Double maxPrecio){
        if (maxPrecio == null){
            return this.servicioProducto.getAll();
        }else {
            return this.servicioProducto.findByPrice(0.0, maxPrecio.doubleValue());
        }
    }

    //GET
    @GetMapping("/getByRangoPrecio")
    public List<Producto> getProductsByRangoPrecio(@RequestParam(required = false) Double maxPrecio){
        if (maxPrecio == null){
            return this.servicioProducto.getAll();
        }else {
            return this.servicioProducto.findByPrecioRango(0.0, maxPrecio.doubleValue());
        }
    }

    //GET
    @GetMapping("/getByPriceProvider")
    public List<Producto> getProductsBypriceProvider(@RequestParam(required = false) Double maxPrecio,
                                                     @RequestParam(required = false) String nombreProveedor){
        if (maxPrecio == null && nombreProveedor != null)
            return this.servicioProducto.findByProvider(nombreProveedor);

        if (maxPrecio == null)
            return this.servicioProducto.findByPrice(0.0, maxPrecio.doubleValue());

        return this.servicioProducto.getAll();
    }

}
