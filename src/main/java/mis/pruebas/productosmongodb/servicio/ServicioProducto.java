package mis.pruebas.productosmongodb.servicio;

import mis.pruebas.productosmongodb.modelo.Producto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ServicioProducto {
    public void addProduct(Producto p);

    public Producto getById(String id);

    public List<Producto> getAll();

    public ResponseEntity updateProducto(Producto producto);

    public ResponseEntity saveProductoById(String idProducto, Producto producto);

    public ResponseEntity delete(Producto producto);

    public List<Producto> findByPrice(double min, double max);

    public List<Producto> findByPrecioRango(double min, double max);

    public List<Producto> findByProvider(String nombreProveedor);
}
